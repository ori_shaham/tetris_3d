﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ShapeCreator : MonoBehaviour
{
    //static lists of all shapeCreators

    public static List<int[,,]> shapes = new List<int[,,]>();
    public static List<Color> colors = new List<Color>();
    public static List<float> odds = new List<float>();

    //var for creating unique shapes on the editor inspector

    public Color color;

    public float odd;

    public int[,,] array = new int [4,4,4];

    //creates 3d array in the inspector for shapes creation

    [Serializable]
    public class CreatorList
    {
        public string[] creator;
    }

    public CreatorList[] creatorList;

    //on start add this shape for the static list

    //the shape is a 3d array with int, where 0 is empty, 1 is a cube, and 2 is a cube that is the pivot of the cube

    void Start()
    {
        //adds color and odds per shape

        colors.Add(color);
        odds.Add(odd);

        //looping through the 3d array of the shape, and converting the string digits to 0,1,2 on the shape axis

        //the 3rd dimension of the array is a string to make easier shape creation from the inspector

        //3d array cant be modify from the inspector

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                for (int k = 0; k < 4; k++)
                {
                    array[i,j,k] = (int)Char.GetNumericValue(creatorList[i].creator[j][k]);
                }
            }
        }
        shapes.Add(array);
    }

}
