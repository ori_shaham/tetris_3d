﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GuiStartMenu : MonoBehaviour
{
    //var for gui buttons
    public int buttonX;
    public int buttonY;
    public static int _buttonX;
    public static int _buttonY;
    public bool buttons = true;

    //var for controllers
    public GameObject controllers;
    bool controlBool = false;

    //var for high score
    public GameObject highScore;
    public static bool scoreBool = false;
    public static int[] scores;
    public static string[] names;
    public GUIStyle style;

    //var for sound gui
    public static bool music = true;
    public static bool sound = true;
    public GameObject objMusic;
    public GameObject objSound;


    void Start ()
    {
        //loading high scores from playerPrefns

        scores = new int[3];
        names = new string[3];

        if (PlayerPrefs.GetString("name0") != null)
        {
            scores[0] = PlayerPrefs.GetInt("score0");
            names[0] = PlayerPrefs.GetString("name0");
            scores[1] = PlayerPrefs.GetInt("score1");
            names[1] = PlayerPrefs.GetString("name1");
            scores[2] = PlayerPrefs.GetInt("score2");
            names[2] = PlayerPrefs.GetString("name2");
        }

        if (scores[0] == 0)
        {
            scores[0] = 80;
            names[0] = "dave";
            scores[1] = 60;
            names[1] = "palgi";
            scores[2] = 30;
            names[2] = "ori";
        }

        // toggle gui view if specific button are pressed
        if (scoreBool)
        {
            buttons = false;
            highScore.SetActive(true);
            objMusic.SetActive(false);
            objSound.SetActive(false);
        }
        else
        {
            objMusic.SetActive(music);
            objSound.SetActive(sound);
        }
     
        //adjust button size to screen size
        _buttonX = Screen.width / buttonX;
        _buttonY = Screen.height / buttonY;
    }

    //if game over, updates highscore table

    public static void UpdateScores(string name, int score)
    {   
        //gets:
        //name of the user
        //score of the user

        scores = new int[3];
        names = new string[3];
        if (PlayerPrefs.GetString("name0") != null)
        {
            scores[0] = PlayerPrefs.GetInt("score0");
            names[0] = PlayerPrefs.GetString("name0");
            scores[1] = PlayerPrefs.GetInt("score1");
            names[1] = PlayerPrefs.GetString("name1");
            scores[2] = PlayerPrefs.GetInt("score2");
            names[2] = PlayerPrefs.GetString("name2");
        }

        //checks if the current score is higher than top three places and adjust positions

        for (int i = 0; i < 3; i++)
        {
            if (score > scores[i])
            {
                if (i == 0)
                {
                    scores[2] = scores[1];
                    names[2] = names[1];
                    scores[1] = scores[0];
                    names[1] = names[0];
                    scores[0] = score;
                    names[0] = name;
                    break;
                }
                else if (i == 1)
                {
                    scores[2] = scores[1];
                    names[2] = names[1];
                    scores[1] = score;
                    names[1] = name;
                    break;
                }
                else
                {
                    scores[2] = score;
                    names[2] = name;
                    break;
                }
            }
        }

        //saves highscore after game over 

        PlayerPrefs.SetString("name0", names[0]);
        PlayerPrefs.SetString("name1", names[1]);
        PlayerPrefs.SetString("name2", names[2]);
        PlayerPrefs.SetInt("score0", scores[0]);
        PlayerPrefs.SetInt("score1", scores[1]);
        PlayerPrefs.SetInt("score2", scores[2]);
        PlayerPrefs.Save();
    }

    //toggle gui view if specific buttons are pressed

    void Update ()
    {
        if (Input.anyKey)
        {
            if (!buttons)
            {
                buttons = true;
                if (controlBool)
                {
                    controllers.SetActive(false);
                    controlBool = false;
                    objMusic.SetActive(music);
                    objSound.SetActive(sound);
                }
                else if (scoreBool)
                {
                    highScore.SetActive(false);
                    scoreBool = false;
                    objMusic.SetActive(music);
                    objSound.SetActive(sound);
                }
            }
        }
    }

    void OnGUI()
    {
        //high score table

        if (scoreBool)
        {
            GUI.Label(new Rect(Screen.width/2 - _buttonX/1.7f, Screen.height/2 - _buttonY/2.5f, _buttonX, _buttonY),
            "1.   " + names[0], style);

            GUI.Label(new Rect(Screen.width/2 - _buttonX/1.7f, Screen.height/2 + _buttonY/2.5f, _buttonX, _buttonY),
            "2.   " + names[1], style);

            GUI.Label(new Rect(Screen.width/2 - _buttonX/1.7f, Screen.height/2 + _buttonY*1.2f, _buttonX, _buttonY),
            "3.   " + names[2], style);

            GUI.Label(new Rect(Screen.width/2 + _buttonX/3, Screen.height/2 - _buttonY/2.5f, _buttonX, _buttonY),
            "" + scores[0], style);

            GUI.Label(new Rect(Screen.width/2 + _buttonX/3, Screen.height/2 + _buttonY/2.5f, _buttonX, _buttonY),
            "" + scores[1], style);

            GUI.Label(new Rect(Screen.width/2 + _buttonX/3, Screen.height/2 + _buttonY*1.2f, _buttonX, _buttonY),
            "" + scores[2], style);
        }

        //gui buttons for start menu

        if (buttons)
        {
            if (GUI.Button(new Rect(Screen.width/2 - _buttonX/2, Screen.height/2 - _buttonY*6, _buttonX, _buttonY), "Play"))
            {
                buttons = false;
                SceneManager.LoadScene(1);
            }

            if (GUI.Button(new Rect(Screen.width/2 - _buttonX/2, Screen.height/2 - _buttonY*4, _buttonX, _buttonY), "Controllers"))
            {
                controllers.SetActive(true);
                controlBool = true;
                buttons = false;
                objMusic.SetActive(false);
                objSound.SetActive(false);
            }

            if (GUI.Button(new Rect(Screen.width/2 - _buttonX/2, Screen.height/2 - _buttonY*2, _buttonX, _buttonY), "Music"))
            {
                if (music)
                {
                    music = false;
                    objMusic.SetActive(false);
                }
                else
                {
                    music = true;
                    objMusic.SetActive(true);
                }

                Music.PlayMusic(music);
            }

            if (GUI.Button(new Rect(Screen.width/2 - _buttonX/2, Screen.height/2, _buttonX, _buttonY), "Sound"))
            {
                if (sound)
                {
                    sound = false;
                    objSound.SetActive(false);
                }
                else
                {
                    sound = true;
                    objSound.SetActive(true);
                }
            }

            if (GUI.Button(new Rect(Screen.width/2 - _buttonX/2, Screen.height/2 + _buttonY*2, _buttonX, _buttonY), "High Score"))
            {
                highScore.SetActive(true);
                scoreBool = true;
                buttons = false;
                objMusic.SetActive(false);
                objSound.SetActive(false);
            }

            if (GUI.Button(new Rect(Screen.width/2 - _buttonX/2, Screen.height/2 + _buttonY*4, _buttonX, _buttonY), "Quit"))
            {
                Application.Quit();
            }
            
        }
    }
}
