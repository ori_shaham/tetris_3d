using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Model : MonoBehaviour
{
    //var for initialize play area
    public View view;
    public Vector3 boardSize = new Vector3 (7, 10, 7);
    public static Vector3 _boardSize;
    private int[,,] playArea;

    // sets where the shape matrix will start on the play area 
    private int startX = 0;
    private int startZ = 0;
    private int startY = 0;

    // x,y,z are the delta between the start of the board size and the start of the shape
    private int x = 0;
    private int z = 0;
    private int y = 0;

    //var for chossing next shape
    private List<Shape> shapes;
    private List<int> odds = new List<int>();
    private Shape currentShape;

    //var for increasing game speed
    public static float waitTime = 1;
    public float startWaitTime = 1;
    public float increasingPerMinute = 0.2f;
    private float time;
    private float deltaTime;
    
    //var for droping shape on y axis by loop
    public static bool active;

    // var for empty full floor
    private List<GameObject> staticToRemove = new List<GameObject>(); 

    // var for rotating shapes
    private Shape rotateShape;
    private Vector3 rPivot;

    //var for sound effect
    public AudioSource emptyRow;
    public AudioSource move;

    //var for score
    public static int score = -5;

    //constructor for Shape

    //the shape is a 3d array with int, where 0 is empty, 1 is a cube, and 2 is a cube that is the pivot of the cube

    public struct Shape 
    {
        public int[,,] array;
        public Color colors;
        public float odds;

        public Shape(int[,,] shape, Color color, float odd) 
        {
            this.array = shape;
            this.colors = color;
            this.odds = odd;
        }
    }

    // first Coroutine of the loop, initialize play area, tells view class to render walls and floor,
    // tells controller to pivot camera around the center

    IEnumerator Start()
    {
        score = -5;
        time = Time.time;
        deltaTime = Time.time; 
        _boardSize = boardSize;
        BuildPlayArea();
        view.Draw();
        Controller.Pivot();
        yield return StartCoroutine(Play(0.1f));
    }

    //adds all the unique shapes that were set in the inspector, calculates odds per shape based on specific shape 
    //variable and calls CreateShape IEnumerator

    IEnumerator Play(float pause)
    {
        yield return new WaitForSeconds(pause);
        AddShapes ();
        OrderOdds ();
        yield return StartCoroutine(CreateShape());
    }

    IEnumerator CreateShape()
    {
        //add 5 point per cycle of shape creation

        score += 5;

        //check time for increasing game speed

        CheckTime();

        //check if there is user filled up a full floor

        CheckFullFloor();

        //choose random shape by the odds calculation

        Shape temp = ChooseShape();

        //build new shape

        currentShape = new Shape(temp.array, temp.colors, temp.odds);
        AddShapeToPlay();

        //sets active bool to true, as long as active is true the game will keep give -1 on the y axis for the shape
        active = true;

        //calls next Coroutine to start dropping the shape
        yield return StartCoroutine(StartDrop());
    }

    IEnumerator StartDrop()
    {
        while (active)
        {
            yield return new WaitForSeconds(waitTime);
            waitTime = startWaitTime;
            if (active)
            {
                View.RemoveCubes();
            }

            // calls drop shape function, that lower the shape by 1 on every waitTime cycle
            DropShape();
        }

        yield return new WaitForSeconds(0.2f);

        //after shape has reached the floor/static cube, gives 0.2 seconds for the player to move the shape under
        //static cubes
        if (!lastChance())
        {
            active = true;
            yield return StartCoroutine(StartDrop());
        }

        else
        {
            FreezeAfterMovement();
        }

        //after shape became static, calls back to CreateShape for the next cycle
        yield return StartCoroutine(CreateShape());
    }
    
    //gives y -1 (y is the height of the lowest part of the shape)

    public void DropShape()
    {
        if (active)
        {
            y--;

            //add yellow cubes that tells user where the shape is projected to land
            AddShadow();

            //looping through the shape, if found a 1 (cube inside the shape) or a 2 (the cube that is the pivot)
            //checks if under the cube there is the floor or a static cube to freeze the shape and make it static

            for (int i = 0; i < 4; i ++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        if (currentShape.array[i, j, k] > 0)
                        {
                            if (y+k > 0)
                            {
                                View.AddCube(new Vector3(x+i, z+j, y+k), currentShape.colors, false);
                                if (playArea[x+i, z+j, y+k-1] == -1)
                                {
                                    FreezeShape();
                                    return;
                                }
                            }
                            else
                            {
                                FreezeShape();
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    // check botton of shape after last chance move 

    bool lastChance()
    {
        for (int i = 0; i < 4; i ++)
        {
            for (int j = 0; j < 4; j++)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (currentShape.array[i, j, k] > 0)
                    {
                        if (y+k > 0)
                        {
                            if (playArea[x+i, z+j, y+k-1] == -1)
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return true; 
                        }
                    }
                }
            }
        }

        return false;
    }


    // freezeshape ends shape movement on the y axis by setting active to false
    // if the shape is above play area, means game over

    void FreezeShape()
    {
        View.RemoveCubes();
        active = false;
        for (int i = 0; i < 4; i ++)
        {
            for (int j = 0; j < 4; j++)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (currentShape.array[i, j, k] > 0)
                    {
                        View.AddCube(new Vector3(x+i, z+j, y+k), currentShape.colors, false);
                        if (y+k >= _boardSize.y)
                        {
                            View.gameOver = true;
                            Time.timeScale = 0; 
                        }           
                    } 
                }
            }
        }
    }

    //after shape has reaced the floor or a static cube, 0.2 seconds will given to the player to still move shape
    //on the x and z axis, after that, render all the cubes with same color and set them to be static cubes
    //looping through the 3d array of the shape, if (currentShape.array[i, j, k] > 0) means there is a cube to render
    //adds playarea -1 where there is a static cube

    void FreezeAfterMovement()
    {
        View.RemoveCubes();
        for (int i = 0; i < 4; i ++)
        {
            for (int j = 0; j < 4; j++)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (currentShape.array[i, j, k] > 0)
                    {
                        View.AddCube(new Vector3(x+i, z+j, y+k), currentShape.colors, true);
                        if (y+k < _boardSize.y)
                        {
                            if (y+k > -1 && i+x > -1 && i+x < _boardSize.x && z+j > -1 && z+j < boardSize.z)
                            {
                                playArea[x+i, z+j, y+k] = -1;
                            }
                        }       
                    } 
                }
            }
        }
    }

    //if gets input from the user, check if moving is possible, than sends view class to render the cubes in the new position

    public void MoveShape(int dir, bool vertical)
    {
        //gets:
        //dir - 1 or -1 for forward or backwards
        //bool vertical if to move it on the x axis or z axis
        if (MovePossible(dir, vertical))
        {
            if (GuiStartMenu.sound)
            {
                move.Play();
            }
            View.RemoveCubes();
            AddShadow();
            for (int i = 0; i < 4; i ++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        if (currentShape.array[i, j, k] > 0)
                        {
                            View.AddCube(new Vector3(x+i, z+j, y+k), currentShape.colors, false);
                        }
                    }
                }
            }
        }
    }

    //check if move is possible by direction and x/z axis

    bool MovePossible (int dir, bool vertical)
    {
        //gets:
        //dir - 1 or -1 for forward or backwards
        //bool vertical if to move it on the x axis or z axis

        //return:
        //
        //if move is possible

        if (vertical)
        {
            return SpecificMove (dir, 0);
        }

        else
        {
            return SpecificMove (0, dir);
        }
    }

    //checking if move is possible by looping through the shape and if cube exists, add delta (x, y, z) and check
    //if cube is outside of play area, or if there is a static cube blocking the way.
    //if true, changes x and z (delta between shape and play area)

    bool SpecificMove (int a, int b)
    {
        //gets:
        //int a - 1 or 0 on the x axis
        //int b - 1 or 0 on the z axis
        //
        //return:
        //if move is possible

        for (int i = 0; i < 4; i ++)
        {
            for (int j = 0; j < 4; j++)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (currentShape.array[i, j, k] > 0)
                    {
                        if (i+x+a < 0 || i+x+a > boardSize.x-1 || j+z+b < 0 || j+z+b > boardSize.z-1)
                        {
                            return false;
                        }

                        if (y+k < _boardSize.y+3 && playArea[x+i+a, z+j+b, y+k] == -1)
                        {
                            return false;
                        }
                    }
                }
            }
        }

        x = x + a;
        z = z + b;
        return true;
    }

    //if gets input from the user, checks if rotating is possible, then rotates the shape around her pivot.
    //the rotate is set by 4 steps:
    //1. transpose the relevante axis, when rotating one axis  is allways not relevante
    //2. reverse each column
    //3. check if possible (out of bounds/hiting static cube)
    //4. adjusting the new position of the shape by the delta between the old pivot and the new

    public void RotateShape(string axis)
    {
        //gets:
        //string axis - "x" or "z" or "y". whicc on axis to rotate shape 
        
        if (RotatePossible(axis))
        {
            if (GuiStartMenu.sound)
            {
                move.Play();
            }
            View.RemoveCubes();

            //if rotate is possible, gives currentshape the after rotation shape array

            currentShape.array = (int[,,])rotateShape.array.Clone();

            //sets new delta

            y = y + (int)rPivot.z;
            x = x + (int)rPivot.x;
            z = z + (int)rPivot.y;

            //rendering new cubes position and shadow

            AddShadow();
            for (int i = 0; i < 4; i ++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        if (currentShape.array[i, j, k] > 0)
                        {
                            View.AddCube(new Vector3(x+i, z+j, y+k), currentShape.colors, false);
                        }
                    }
                }
            }

        }
    }

    //checks if rotation is possible, if all new cube positions are okay after looping through the shape, return true

    bool RotatePossible (string axis)
    {
        //gets:
        //string axis - "x" or "z" or "y"
        //return:
        //true if rotation is possible

        //checks where is the pivot
        Vector3 v3 = GetPivot();

        //rotate the shape before checking if okay
        ShapeRotation (v3, axis);

        //check if in bounds and no blocking cubes interrupted in the way 
        for (int i = 0; i < 4; i ++)
        {
            for (int j = 0; j < 4; j++)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (rotateShape.array[i,j,k] > 0)
                    {
                        if (k+y+(int)rPivot.z > _boardSize.y+3 || i+x+(int)rPivot.x < 0 ||
                            i+x+(int)rPivot.x > _boardSize.x-1 || j+z+(int)rPivot.y < 0 ||
                            j+z+(int)rPivot.y > _boardSize.z-1 || k+y+(int)rPivot.z < 0 ||
                            playArea[i+x+(int)rPivot.x, j+z+(int)rPivot.y, k+y+(int)rPivot.z] == -1)
                        {
                            print ("out of bounds");
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    //rotate the shape by transpose the array and reverse the rows, sets pivot delta for new shape position

    void ShapeRotation (Vector3 pivot, string axis)
    {
        //gets:
        //vector old pivot
        //string axis - "x" or "z" or "y"

        switch (axis)
        {
        case "x":
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        //transpose and reverse
                        rotateShape.array[i, j, k] = currentShape.array[4-j-1, i, k];
                        rotateShape.colors = currentShape.colors;
                        
                        //check for the new pivot and sets pivot delta
                        if (rotateShape.array[i, j, k] == 2)
                        {
                            rPivot = new Vector3(pivot.x-i, pivot.y-j, 0);
                        }
                    }
                }
            }
            return;

        case "z":
            
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        rotateShape.array[i, j, k] = currentShape.array[i, 4-k-1, j];
                        rotateShape.colors = currentShape.colors;
                        
                        if (rotateShape.array[i, j, k] == 2)
                        {
                            rPivot = new Vector3(0, pivot.y-j, pivot.z-k);
                        }
                    }
                }
            }
            return;

        case "y":
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        rotateShape.array[i, j, k] = currentShape.array[k, j, 4-i-1];
                        rotateShape.colors = currentShape.colors;
                        
                        if (rotateShape.array[i, j, k] == 2)
                        {
                            rPivot = new Vector3(pivot.x-i, 0, pivot.z-k);
                        }
                    }
                }
            }
            return;
        }
    }

    //looping through the shape, if 2 is found, that is the pivot

    Vector3 GetPivot()
    {
        for (int i = 0; i < 4; i ++)
        {
            for (int j = 0; j < 4; j++)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (currentShape.array[i, j, k] == 2)
                    {
                        return new Vector3 (i, j, k);
                    }
                }
            }
        }

        return Vector3.zero;
    }


    //every 60 seconds increase game speed by lowering startWaitTime variable by the increasingPerMinute variable set
    //in the inspector

    void CheckTime()
    {
        if (startWaitTime > increasingPerMinute)
        {
            deltaTime = Time.time;
            if (deltaTime > time + 60)
            {
                deltaTime = Time.time;
                time = Time.time;
                startWaitTime -= increasingPerMinute;
            }
        }
    }

    //each cycle, loop through the play area, if entire level (k) is filled, destroy all cubes and gives
    //user +25 points by calling EmptyFullFloor function
    //if a 0 is found, get out of the loop and go to next level

    void CheckFullFloor()
    {
        bool fullFloor = true;
        for (int k = 0; k < _boardSize.y; k++)
        {
            fullFloor = true;
            for (int j = 0; j < _boardSize.z; j++)
            {
                for (int i = 0; i < _boardSize.x; i++)
                {
                    if (playArea[i, j, k] != -1)
                    {
                        j = (int)_boardSize.z;
                        fullFloor = false;
                        break;
                    }
                }
            }   
            if (fullFloor)
            {
                EmptyFullFloor(k);
                k--;
            }
        }
    }

    //empty a full level of cubes, from that specific level go above and lower all the static cubes
    //add 25 points to score

    void EmptyFullFloor(int h)
    {
        //gets:
        //h - height of the level to destroy
        bool empty = false;

        for (int k = 0; k < _boardSize.y-1; k++)
        {
            if (k == h)
            {
                if (GuiStartMenu.sound)
                {
                    emptyRow.Play();
                }
                score += 25;
                empty = true;
                foreach (GameObject obj in View.staticCubes)
                {
                    if (obj != null)
                    {
                        //if cube is on the filled level, destory
                        if (obj.transform.position.y == h)
                        {
                            Destroy(obj);
                            staticToRemove.Add(obj);
                        }
                        //if cube is above the filled level, lower by one
                        else if (obj.transform.position.y > h)
                        {
                            obj.transform.position = new Vector3 (obj.transform.position.x, obj.transform.position.y-1,
                            obj.transform.position.z); 
                        }
                    }
                }
            }

            //adjust play area to the new static cubes position

            if (empty)
            {
                for (int j = 0; j < _boardSize.z; j++)
                {
                    for (int i = 0; i < _boardSize.x; i++)
                    {
                        playArea[i, j, k] = playArea[i, j, k+1];
                    }
                }
            }
        }
    }

    //loop through the y axis from top to bottom and the whole shape,
    // if cube is found, render shadow cube at the projected final location

    public void AddShadow()
    {
        for (int l = y-1; l > -5; l--)
        {   
            for (int i = 0; i < 4; i ++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        if (currentShape.array[i, j, k] > 0)
                        {
                            if (l+k < _boardSize.y)
                            {
                                if (l+k > 0)
                                {
                                    //if a cube and the next static cube beneath it has found, call
                                    //freeze shadow to render the shadow
                                    if (playArea[x+i, z+j, l+k-1] == -1)
                                    {
                                        FreezeShadow(l);
                                        return;
                                    }
                                }
                                else
                                {
                                    FreezeShadow(l);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //render the shadow

    void FreezeShadow(int l)
    {
        //gets:
        //int l that is the delta between the cube and her projected y position
        
        View.RemoveShadows();
        for (int i = 0; i < 4; i ++)
        {
            for (int j = 0; j < 4; j++)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (currentShape.array[i, j, k] > 0)
                    {
                        View.AddShadow(new Vector3(x+i, z+j, l+k));
                    } 
                }
            }
        }
    }
   
    //adds a shape to play area after a cycle has done
    //rendering cubes at start position (above the play area)
    
    void AddShapeToPlay()
    {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (currentShape.array[i, j, k] != 0)
                    {
                        View.AddCube(new Vector3(startX+i, startZ+j, startY+k), currentShape.colors, false);
                    }
                }
            }
        }

        x = startX;
        z = startZ;
        y = startY;
        AddShadow();
    }

    //convert board size to play area, adding above the board size space for the shape to be created
    //sets start position for the shape at the middle of the board size 

    void BuildPlayArea()
    {
        playArea = new int[(int)_boardSize.x, (int)_boardSize.z, (int)_boardSize.y+6];
        startX = (int)(_boardSize.x/2);
        startZ = (int)(_boardSize.z/2) - 1;
        startY = (int)(_boardSize.y - 1);
    }

    //generates random shape by odds probability

    Shape ChooseShape ()
    {
        int rnd = Random.Range(0, odds.Count);
        return shapes[odds[rnd]];
    }

    //sets odds per shape

    void OrderOdds ()
    {
        int total = 100;
        int count = 0;
        int times = 0;

        //if a shape odd is set (above 0), add it to the odds list by multiple its odd by 100
        //e.g if a shape odds to genereate is 0.2, add it to the list 20 times

        for (int i = 0; i < shapes.Count; i++)
        {
            if (shapes[i].odds != 0)
            {
                times = (int)(shapes[i].odds * 100);
                total -= times;
                for (int j = 0; j < times; j++)
                {
                    odds.Add(i);
                }
                count++;
            }
        }

        //if all the odds of the shapes are above 100%, sets equal odd for every shape by adding every shape to the list
        //just one time

        if (total < shapes.Count - count)
        {
            odds.Clear();
            for (int i = 0; i < shapes.Count; i++)
            { 
                odds.Add(i);
            }
        }

        //if not all the odds are above 100%, give the remaining shapes equal odds to generate by the remainder of the odds
        else
        {
            times = (int) (total / (shapes.Count-count)); 
            for (int i = 0; i < shapes.Count; i++)
            {
                if (shapes[i].odds == 0)
                {
                    for (int j = 0; j < times; j++)
                    {
                        odds.Add(i);
                    }
                }
            }
            while (odds.Count < 100)
            {
                odds.Add(Random.Range(0, shapes.Count));
            }
        }
    }

    //generates a list of the shapes from the static ShapeCreator.shapes list

    void AddShapes ()
    {
        shapes = new List<Shape>();

        for (int i = 0; i < ShapeCreator.shapes.Count; i++)
        {
            shapes.Add(new Shape(ShapeCreator.shapes[i], ShapeCreator.colors[i], ShapeCreator.odds[i]));
        }
        
        //set an instance of a shape for the rotate shape to not be null
        rotateShape = new Shape(new int[4,4,4], Color.red, 0);
    }
}
