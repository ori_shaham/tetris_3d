﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    //var for camera movement

    public float sensitivity;
    public float xAngleMax = 65;
    public float xAngleMin = 0;
    public static float camAngle;
    public static Vector3 pivot;

    //var for shape movement

    public string camDir = "front";
    public GameObject model;

    //var for sound effect
    public AudioSource move; 

    void Update ()
    {
        camAngle = transform.eulerAngles.y;

        MoveCamera ();

        CameraAngle();

        MoveShape();

        RotateShape();
    }

    // sets camera to allways lookat the center of the play area

    public static void Pivot()
    {
        pivot = Model._boardSize / 2;
        pivot.y = 0;
        Camera.main.transform.position = new Vector3(pivot.x, 10.5f, pivot.z - 9.7f);
    }

    //input for rotating shapes, checks angle of the camera to convert 3d pov from the user to the play area

    void RotateShape()
    {
        if (Input.GetKeyDown("x"))
        {
            model.GetComponent<Model>().RotateShape("x");
        }

        if (Input.GetKeyDown("z"))
        {
            switch (camDir)
            {
            case "front":
                model.GetComponent<Model>().RotateShape("z");
                break;

            case "back":
                model.GetComponent<Model>().RotateShape("z");
                break;
            
            case "right":
                model.GetComponent<Model>().RotateShape("y");
                break;

            case "left":
                model.GetComponent<Model>().RotateShape("y");
                break;
            }
        }

        if (Input.GetKeyDown("c"))
        {
            switch (camDir)
            {
            case "front":
                model.GetComponent<Model>().RotateShape("y");
                break;

            case "back":
                model.GetComponent<Model>().RotateShape("y");
                break;
            
            case "right":
                model.GetComponent<Model>().RotateShape("z");
                break;

            case "left":
                model.GetComponent<Model>().RotateShape("z");
                break;
            }
        }
    }

    //input for moving shapes, checks angle of the camera to convert 3d pov from the user to the play area

    void MoveShape()
    {
        if (Model.active && Input.GetKeyDown("space"))
        {
            View.RemoveCubes();
            model.GetComponent<Model>().DropShape();
            if (GuiStartMenu.sound)
            {
                move.Play();
            }
        }

        if (Input.GetKeyDown("w") || Input.GetKeyDown("up"))
        {
            switch (camDir)
            {
            case "front":
                model.GetComponent<Model>().MoveShape(1, false);
                break;

            case "back":
                model.GetComponent<Model>().MoveShape(-1, false);
                break;
            
            case "right":
                model.GetComponent<Model>().MoveShape(-1, true);
                break;

            case "left":
                model.GetComponent<Model>().MoveShape(1, true);
                break;
            }
        }

        if (Input.GetKeyDown("s") || Input.GetKeyDown("down"))
        {
            switch (camDir)
            {
            case "front":
                model.GetComponent<Model>().MoveShape(-1, false);
                break;

            case "back":
                model.GetComponent<Model>().MoveShape(1, false);
                break;
            
            case "right":
                model.GetComponent<Model>().MoveShape(1, true);
                break;

            case "left":
                model.GetComponent<Model>().MoveShape(-1, true);
                break;
            }
        }

        if (Input.GetKeyDown("d") || Input.GetKeyDown("right"))
        {
            switch (camDir)
            {
            case "front":
                model.GetComponent<Model>().MoveShape(1, true);
                break;

            case "back":
                model.GetComponent<Model>().MoveShape(-1, true);
                break;
            
            case "right":
                model.GetComponent<Model>().MoveShape(1, false);
                break;

            case "left":
                model.GetComponent<Model>().MoveShape(-1, false);
                break;
            }
        }

        if (Input.GetKeyDown("a") || Input.GetKeyDown("left"))
        {
            switch (camDir)
            {
            case "front":
                model.GetComponent<Model>().MoveShape(-1, true);
                break;

            case "back":
                model.GetComponent<Model>().MoveShape(1, true);
                break;
            
            case "right":
                model.GetComponent<Model>().MoveShape(-1, false);
                break;

            case "left":
                model.GetComponent<Model>().MoveShape(1, false);
                break;
            }
        }
    }

    //input of the mouse movement to move camera, limits camera angle from 0 to 65;

    void MoveCamera ()
    {
        float rotateHorizontal = Input.GetAxis ("Mouse X") * sensitivity;
        float rotateVertical = Input.GetAxis ("Mouse Y") * sensitivity;
        transform.RotateAround (pivot, Vector3.up, rotateHorizontal); 

        if (transform.eulerAngles.x > xAngleMin && transform.eulerAngles.x < xAngleMax)
        {
            transform.RotateAround (pivot, -transform.right, rotateVertical);  
        }
        else 
        {
            if (transform.eulerAngles.x < xAngleMin || transform.eulerAngles.x > xAngleMax+30)
            {
                transform.RotateAround (pivot, -transform.right, -0.1f);  
            }
            else if (transform.eulerAngles.x > xAngleMax)
            {
                transform.RotateAround (pivot, -transform.right, 0.1f);  
            }
        }
    }

    //check current camera angle

    void CameraAngle()
    {
        if (camAngle < 45 || camAngle > 314 )
        {
            camDir = "front";
        }
        else if (camAngle < 315 && camAngle > 224)
        {
            camDir = "right";
        }
        else if (camAngle < 225 && camAngle > 134)
        {
            camDir = "back";
        }
        else if (camAngle < 135 && camAngle > 44)
        {
            camDir = "left";
        }
    }
}
