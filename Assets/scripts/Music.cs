﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    private static Music musicInstance;
    public static AudioSource audio; 
    
    //keep music running between scenes, checks for duplicate music objects

    void Awake()
    {
        DontDestroyOnLoad (this);

        if (musicInstance == null) 
        {
            musicInstance = this;
        }

        else 
        {
            DestroyObject(gameObject);
        }
    }

    void Start()
    {
        audio = gameObject.GetComponent<AudioSource>();
    }

    //toggle music on and off

    public static void PlayMusic(bool music)
    {
        if (music)
        {
            audio.Play();
        }
        else
        {
            audio.Stop();
        }
    }
}
