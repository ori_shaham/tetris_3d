﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class View : MonoBehaviour
{
    //var for building floor and wall at the begining

    public GameObject floor;
    public GameObject grid;
    public GameObject frontWall;
    public GameObject leftWall;
    public GameObject backWall;
    public GameObject rightWall;
    public GameObject empty;
    float angle;

    //var for converting board size to 3d view
    
    private Vector3 boardPos;

    //var for creating cubes

    public GameObject prefab;
    public static GameObject _prefab;
    public static List<GameObject> cubes = new List<GameObject>();
    public static List<GameObject> staticCubes = new List<GameObject>();
    public static Transform trans;

    //var for shape shadow

    public GameObject shadow;
    public static GameObject _shadow;
    public static List<GameObject> shadows = new List<GameObject>();

    //var for gui

    public GUIStyle style;
    public GUIStyle userStyle;
    public GUIStyle gameOverStyle;
    private bool buttons1 = true;
    private bool buttons2 = false;
    public int buttonX;
    public int buttonY;
    public static int _buttonX;
    public static int _buttonY;
    public static bool gameOver = false;
    public static string userName = "";
    private bool userReturn = false;

    //after model builds play area, rendering floor and walls grid

    public void Draw()
    {
        _buttonX = Screen.width / buttonX;
        _buttonY = Screen.height / buttonY;

        trans = transform;

        _prefab = prefab;

        _shadow = shadow;

        ConvertBoardPos();

        BuildFloor();

        frontWall = BuildWall ("frontWall", Model._boardSize, -0.5f, true);

        backWall = BuildWall ("backWall", Model._boardSize, Model._boardSize.z-0.5f, true);

        leftWall = BuildWall ("leftWall", Model._boardSize, -0.5f, false);

        rightWall = BuildWall ("rightWall", Model._boardSize, Model._boardSize.x-0.5f, false);
    }

    // deactive walls when the wall blocks the angle of the camera 

    void Update()
    {
        DeactivateWalls();
    }

    //instantiate cube with color to a specific vector3 send by the model, bool stati means if the cube
    //is static or still moving

    public static void AddCube (Vector3 v3, Color color, bool stati)
    {
        //gets:
        //vector3 for position
        //color of the cube
        //bool if static or not

        GameObject cube = Instantiate(_prefab, new Vector3(v3.x, v3.z, v3.y), Quaternion.identity);
        var cubeRenderer = cube.GetComponent<Renderer>();
        cubeRenderer.material.SetColor("_Color", color);
        if (stati)
        {
            cube.transform.parent = trans; 
            staticCubes.Add(cube);
        }
        else
        {
            cubes.Add(cube);
        }
    }

    //instantiate yellow cubes to mark the user where the shape will land eventually

    public static void AddShadow (Vector3 v3)
    {
        //gets:
        //vector3 for position

        GameObject cube = Instantiate(_shadow, new Vector3(v3.x, v3.z, v3.y), Quaternion.identity);
        cube.transform.parent = trans; 
        shadows.Add(cube);
    }

    //add the end of the DropShape model loop, destroy all cubes and waits for the next loop rendering

    public static void RemoveCubes ()
    {
        foreach(GameObject obj in cubes)
        {
            Destroy(obj);
        }
        cubes.Clear();

        foreach(GameObject obj in shadows)
        {
            Destroy(obj);
        }
        shadows.Clear();
    }

    public static void RemoveShadows ()
    {
        foreach(GameObject obj in shadows)
        {
            Destroy(obj);
        }
        shadows.Clear();
    }

    //build walls by the size of the custom board size set in the inspector

    GameObject BuildWall (string name, Vector3 matrix, float start, bool face)
    {
        //gets:
        //name of the wall (front, back, right, left)
        //vector3 size of the board
        //float for where to start bulding the wall
        //bool face - true if facing the user (front and back) or false if vertical (right wall or left wall)

        GameObject g = new GameObject(name);
        g = Instantiate(g, new Vector3(0, 0 ,0), Quaternion.identity);
        GameObject temp;

        //looping through the board size to add grid
        if (face)
        {
            for (int i = 0; i < matrix.x; i++)
            {
                for (int j = 0; j < matrix.y; j++)
                {
                    temp = Instantiate(grid, new Vector3(i, j, start), Quaternion.Euler(0, 0, 0));
                    temp.transform.parent = g.transform; 
                }
            }
        }

        else
        {
            for (int i = 0; i < matrix.z; i++)
            {
                for (int j = 0; j < matrix.y; j++)
                {
                    temp = Instantiate(grid, new Vector3(start, j, i), Quaternion.Euler(0, 90, 0));
                    temp.transform.parent = g.transform; 
                }
            }        
        }

        return g;
    }
    
    //where to instantiate the floor

    void ConvertBoardPos()
    {
        boardPos = Model._boardSize / 2 - new Vector3(0.5f, 0.5f, 0.5f);
        boardPos.y = -0.5f;
    }

    //build the floor with plane and grid

    void BuildFloor()
    {
        GameObject temp = Instantiate(floor, boardPos, Quaternion.Euler(0, 0, 0));
        temp.transform.localScale = Model._boardSize / 10;

        for (int i = 0; i < Model._boardSize.x; i++)
        {
            for (int j = 0; j < Model._boardSize.z; j++)
            {
                temp = Instantiate(grid, new Vector3(i, -0.42f, j), Quaternion.Euler(90, 0, 0));
                temp.transform.parent = empty.transform; 
            }
        }
    }

    //if wall blocks camera angle, deactivate wall

    void DeactivateWalls()
    {
        angle = Controller.camAngle;

        if (angle < 50 || angle > 310)
        {
            frontWall.SetActive(false);
        } 
        else
        {
            frontWall.SetActive(true);
        }

        if (angle > 40 && angle < 140)
        {
            leftWall.SetActive(false);
        } 
        else
        {
            leftWall.SetActive(true);
        }

        if (angle > 130 && angle < 230)
        {
            backWall.SetActive(false);
        } 
        else
        {
            backWall.SetActive(true);
        }

        if (angle > 220 && angle < 320)
        {
            rightWall.SetActive(false);
        } 
        else
        {
            rightWall.SetActive(true);
        }
    }    
    
    void OnGUI()
    {
        // sets right corner score label

        GUI.Label(new Rect(Screen.width - _buttonX, 10, _buttonX, _buttonY), "Score: " + Model.score, style);

        //if game over, pause game, write game over and final score, and asks player for name for the high score table

        if (gameOver)
        {
            buttons1 = false;
            Time.timeScale = 0;  
            GUI.Label(new Rect(Screen.width/2 - _buttonX, Screen.height/2 - _buttonY*4, 
            _buttonX, _buttonY), "Game Over", gameOverStyle);

            GUI.Label(new Rect(Screen.width/2 - _buttonX*1.75f, Screen.height/2 - _buttonY, 
            _buttonX, _buttonY), "Your final score is: " + Model.score + "!" , gameOverStyle);

            GUI.Label(new Rect(Screen.width/2 - _buttonX*1.5f, Screen.height/2 + _buttonY, 
            _buttonX, _buttonY), "What is your name?" , gameOverStyle);

            userName = GUI.TextField(new Rect(Screen.width/2 - _buttonX/1.5f, Screen.height/2 + _buttonY*4, 
            _buttonX, _buttonY), userName, 25, userStyle);

            //after "enter" button pressed, update high score table, and load start menu scene with open updated
            //high score table

            Event e = Event.current;
            if (e.keyCode == KeyCode.Return)
            {
                GuiStartMenu.UpdateScores(userName, Model.score);
                gameOver = false;
                Time.timeScale = 1;  
                GuiStartMenu.scoreBool = true;
                SceneManager.LoadScene(0);
            }
        }

        // gui buttons on play screen, restart level and pause

        if (buttons1)
        {
            if (GUI.Button(new Rect(10, 10, 60, 40), "restart"))
            {
                SceneManager.LoadScene(1);
            }

            if (GUI.Button(new Rect(10, 60, 60, 40), "pause"))
            {
                Time.timeScale = 0;   
                buttons1 = false;
                buttons2 = true;
            }
        }

        // if player pressed pause, pause game and add "back to game" or "exit to start menu" buttons

        if (buttons2)
        {
            if (GUI.Button(new Rect(Screen.width/2 - _buttonX/2, Screen.height/2 - _buttonY*4, _buttonX, _buttonY),
                "Back to game"))
            {
                Time.timeScale = 1;   
                buttons1 = true;
                buttons2 = false;
            }

            if (GUI.Button(new Rect(Screen.width/2 - _buttonX/2, Screen.height/2 - _buttonY*2, _buttonX, _buttonY),
                "Exit to start menu"))
            {
                Time.timeScale = 1;   
                SceneManager.LoadScene(0);
            }
        }
    }
}